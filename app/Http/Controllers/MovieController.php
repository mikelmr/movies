<?php

namespace App\Http\Controllers;

use App\Factories\FilterFactory;
use App\Repositories\MoviesRepository;

class MovieController extends Controller
{
    function index(MoviesRepository $moviesRepository)
    {
        $filters = FilterFactory::make(request()->all());
        foreach ($filters as $filter) {
            $moviesRepository->addFilter($filter);
        }
        $movies = $moviesRepository->filter($filters);
        return view('movies', compact('movies'));
    }
}

<?php

namespace App\Repositories;

use App\Filters\IFilter;

class MoviesRepository
{
    private Array $movies;
    private Array $filters = [];

    public function __construct(Array $movies) {
        $this->movies = $movies;
    }

    public function filter() : Array 
    {
        $filteredMovies = $this->movies;

        foreach ($this->filters as $filter) {
            $filteredMovies = $filter->apply($filteredMovies);
        }
        return $filteredMovies;
    }

    public function addFilter(IFilter $filter) : void 
    {
        $this->filters[] = $filter;
    }
}
<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Factories\FilterFactory;
use App\Repositories\MoviesRepository;

class MovieCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'movie:list {--year=} {--title=} {--ftitle=} {--rating=} {--frating=}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Devuelve el listado de las películas';

    private MoviesRepository $moviesRepository;

    public function __construct(MoviesRepository $moviesRepository)
    {
        parent::__construct();
        $this->moviesRepository = $moviesRepository;
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $headers = ['Titulo', 'Año', 'Rating'];

        $this->line(implode(';', $headers));

        $filters = FilterFactory::make($this->options());
        foreach ($filters as $filter) {
            $this->moviesRepository->addFilter($filter);
        }

        $movies = $this->moviesRepository->filter();
        foreach ($movies as $movie) {
            $this->line(implode(';', $movie));
        }

        return 0;
    }
}

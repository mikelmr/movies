<?php

namespace App\Factories;

use App\Filters\YearFilter;
use App\Filters\TitleFilter;
use App\Filters\RatingFilter;
use App\Enums\TitleOperatorsEnum;
use App\Enums\RatingOperatorsEnum;

class FilterFactory
{
    private static function isInteger(array $parameters, string $key): bool
    {
        if (!isset($parameters[$key])) {
            return false;
        }

        return(ctype_digit(strval($parameters[$key]))); 
    }
    public static function make(array $parameters): array
    {
        $filters = [];

        if (self::isInteger($parameters, 'year')) {
            $filters[] = new YearFilter((int)$parameters['year']);
        }

        if (
            isset($parameters['ftitle'])
            && isset($parameters['title'])
            && $operator = TitleOperatorsEnum::fromValue($parameters['ftitle'])
        ) {
            $filters[] = new TitleFilter($parameters['title'], $operator);
        }

        if (
            isset($parameters['frating'])
            && self::isInteger($parameters, 'rating')
            && $operator = RatingOperatorsEnum::fromValue($parameters['frating'])
        ) {
            $filters[] = new RatingFilter((int)$parameters['rating'], $operator);
        }

        return $filters;
    }
}

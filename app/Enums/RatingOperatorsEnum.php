<?php

namespace App\Enums;

enum RatingOperatorsEnum: string
{
    case LESSER_OR_EQUALS = 'le';
    case GREATER_OR_EQUAL = 'ge';

    public static function fromValue(string $value): ?RatingOperatorsEnum
    {
        switch ($value) {
            case self::LESSER_OR_EQUALS->value:
                return self::LESSER_OR_EQUALS;
            case self::GREATER_OR_EQUAL->value:
                return self::GREATER_OR_EQUAL;
            default:
                return null;
        }
    }
}

<?php

namespace App\Enums;

enum TitleOperatorsEnum: string
{
    case CONTAINS = 'c';
    case STARTS_WITH = 'sw';
    case ENDS_WITH = 'ew';

    public static function fromValue(string $value): ?TitleOperatorsEnum
    {
        switch ($value) {
            case self::CONTAINS->value:
                return self::CONTAINS;
            case self::STARTS_WITH->value:
                return self::STARTS_WITH;
            case self::ENDS_WITH->value:
                return self::ENDS_WITH;
            default:
                return null;
        }
    }
}

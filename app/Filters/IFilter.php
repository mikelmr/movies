<?php

namespace App\Filters;

interface IFilter
{
    public function apply(Array $value): Array;
}
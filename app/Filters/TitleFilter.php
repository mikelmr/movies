<?php

namespace App\Filters;

use App\Enums\TitleOperatorsEnum;

class TitleFilter implements IFilter
{
    private string $value;
    private TitleOperatorsEnum $operator;

    public function __construct(string $value, TitleOperatorsEnum $operator) {
        $this->value = $value;
        $this->operator = $operator;
    }

    function apply(Array $movies) : Array 
    {
        return array_values(array_filter($movies, function($movie) {
            switch ($this->operator) {
                case TitleOperatorsEnum::CONTAINS:
                    return stripos($movie['title'], $this->value) !== false;
                case TitleOperatorsEnum::STARTS_WITH:
                    return stripos($movie['title'], $this->value) === 0;
                case TitleOperatorsEnum::ENDS_WITH:
                    $pos = strlen($movie['title']) - strlen($this->value);
                    return strpos($movie['title'], $this->value, $pos) !== false;
            }
        }));
    }
}
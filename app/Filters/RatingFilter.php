<?php

namespace App\Filters;

use App\Enums\RatingOperatorsEnum;

class RatingFilter implements IFilter
{
    private int $value;
    private RatingOperatorsEnum $operator;

    public function __construct(int $value, RatingOperatorsEnum $operator) {
        $this->value = $value;
        $this->operator = $operator;
    }

    function apply(Array $movies) : Array 
    {
        return array_values(array_filter($movies, function($movie) {
            switch ($this->operator) {
                case RatingOperatorsEnum::LESSER_OR_EQUALS:
                    return $movie['rating'] <= $this->value;
                case RatingOperatorsEnum::GREATER_OR_EQUAL:
                    return $movie['rating'] >= $this->value;
            }
        }));
    }
}
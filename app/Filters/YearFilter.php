<?php

namespace App\Filters;

class YearFilter implements IFilter
{
    private int $value;

    public function __construct(int $value) {
        $this->value = $value;
    }

    function apply(Array $movies) : Array 
    {
        return array_values(array_filter($movies, function($movie) {
            return $movie['year'] === $this->value;
        }));
    }
}
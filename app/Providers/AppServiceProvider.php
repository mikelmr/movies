<?php

namespace App\Providers;

use App\Repositories\MoviesRepository;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        $this->app->bind(MoviesRepository::class, function ($app) {
            $movies = json_decode(file_get_contents(base_path('database/movies.json')), true);;
            return new MoviesRepository($movies);
        });
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}

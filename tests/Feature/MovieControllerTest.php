<?php

namespace Tests\Feature;

use App\Repositories\MoviesRepository;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class MovieControllerTest extends TestCase
{
    private Array $movies;

    function setUp(): void
    {
        parent::setUp();

        $this->movies = [
            ["title" => "ABC", "year" => 2022, "rating" => 5],
            ["title" => "BCD", "year" => 2023, "rating" => 7],
            ["title" => "CDE", "year" => 2024, "rating" => 10],
        ];

        $this->app->instance(MoviesRepository::class, new MoviesRepository($this->movies));
    }

    /** @test */
    public function it_should_display_the_header(): void
    {
        $response = $this->get('/');

        $response->assertStatus(200);
        $response->assertSee('Lista de Películas');
    }

    /** @test */
    public function it_should_display_movies_title_year_and_rating(): void
    {
        $response = $this->get('/');

        $response->assertStatus(200);
        foreach ($this->movies as $movie) {
            $response->assertSee($movie['title']);
            $response->assertSee($movie['year']);
            $response->assertSee($movie['rating']);
        }
    }

    /** @test */
    public function it_should_filter_year(): void
    {
        $response = $this->get('/?year=2022');

        $response->assertStatus(200);
        $response->assertSee(2022);
        $response->assertDontSee(2023);
        $response->assertDontSee(2024);
    }

    /** @test */
    public function it_should_filter_title(): void
    {
        $response = $this->get('/?ftitle=sw&title=A');

        $response->assertStatus(200);
        $response->assertSee('ABC');
        $response->assertDontSee('BCD');
        $response->assertDontSee('CDE');
    }

    /** @test */
    public function it_should_filter_rating(): void
    {
        $response = $this->get('/?frating=le&rating=7');

        $response->assertStatus(200);
        $response->assertSee('ABC');
        $response->assertSee('BCD');
        $response->assertDontSee('CDE');
    }
}

<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Repositories\MoviesRepository;

class MovieCommandTest extends TestCase
{
    private array $movies;

    function setUp(): void
    {
        parent::setUp();

        $this->movies = [
            ["title" => "ABC", "year" => 2022, "rating" => 5],
            ["title" => "BCD", "year" => 2023, "rating" => 7],
            ["title" => "CDE", "year" => 2024, "rating" => 10],
        ];

        $this->app->instance(MoviesRepository::class, new MoviesRepository($this->movies));
    }

    /** @test */
    public function it_should_display_the_header_and_movies(): void
    {
        $this->artisan('movie:list')
            ->expectsOutputToContain('Titulo;Año;Rating')
            ->expectsOutputToContain('ABC;2022;5')
            ->expectsOutputToContain('BCD;2023;7')
            ->expectsOutputToContain('CDE;2024;10')
            ->assertExitCode(0);
    }

    /** @test */
    public function it_should_filter_year(): void
    {
        $this->artisan('movie:list', ['--year' => 2023])
            ->expectsOutputToContain('Titulo;Año;Rating')
            ->doesntExpectOutputToContain('ABC;2022;5')
            ->expectsOutputToContain('BCD;2023;7')
            ->doesntExpectOutputToContain('CDE;2024;10')
            ->assertExitCode(0);
    }
    
    /** @test */
    public function it_should_filter_title(): void
    {
        $this->artisan('movie:list', ['--title' => 'A', '--ftitle' => 'sw'])
            ->expectsOutputToContain('Titulo;Año;Rating')
            ->expectsOutputToContain('ABC;2022;5')
            ->doesntExpectOutputToContain('BCD;2023;7')
            ->doesntExpectOutputToContain('CDE;2024;10')
            ->assertExitCode(0);
    }
    
    /** @test */
    public function it_should_filter_rating(): void
    {
        $this->artisan('movie:list', ['--rating' => 7, '--frating' => 'le'])
            ->expectsOutputToContain('Titulo;Año;Rating')
            ->expectsOutputToContain('ABC;2022;5')
            ->expectsOutputToContain('BCD;2023;7')
            ->doesntExpectOutputToContain('CDE;2024;10')
            ->assertExitCode(0);
    }
}

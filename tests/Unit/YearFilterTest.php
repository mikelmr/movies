<?php

namespace Tests\Unit;

use App\Filters\YearFilter;
use PHPUnit\Framework\TestCase;

class YearFilterTest extends TestCase
{
    private Array $movies;

    function setUp(): void
    {
        parent::setUp();

        $this->movies = [
            ["title" => "ABC", "year" => 2022, "rating" => 5],
            ["title" => "BCD", "year" => 2023, "rating" => 7],
            ["title" => "CDE", "year" => 2024, "rating" => 10],
        ];
    }
    
    /** @test */
    public function filter_contains_bc(): void
    {
        $yearFilter = new YearFilter(2022);
        $filteredMovies = $yearFilter->apply($this->movies);

        $this->assertEquals([["title" => "ABC", "year" => 2022, "rating" => 5]], $filteredMovies);
    }
}

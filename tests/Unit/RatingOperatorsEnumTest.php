<?php

namespace Tests\Unit;

use App\Enums\RatingOperatorsEnum;
use PHPUnit\Framework\TestCase;

class RatingOperatorsEnumTest extends TestCase
{
    /** @test */
    public function if_valid_value_returns_ok_with_valid_value(): void
    {
        $fromValue = RatingOperatorsEnum::fromValue(RatingOperatorsEnum::LESSER_OR_EQUALS->value);
        $this->assertEquals(RatingOperatorsEnum::LESSER_OR_EQUALS, $fromValue);
    }

    /** @test */
    public function if_not_valid_value_returns_null(): void
    {
        $fromValue = RatingOperatorsEnum::fromValue('NOT_VALID');
        $this->assertNull($fromValue);
    }
}

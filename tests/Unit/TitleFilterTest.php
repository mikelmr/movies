<?php

namespace Tests\Unit;

use App\Filters\TitleFilter;
use PHPUnit\Framework\TestCase;
use App\Enums\TitleOperatorsEnum;

class TitleFilterTest extends TestCase
{
    private Array $movies;

    function setUp(): void
    {
        parent::setUp();

        $this->movies = [
            ["title" => "ABC", "year" => 2022, "rating" => 5],
            ["title" => "BCD", "year" => 2023, "rating" => 7],
            ["title" => "CDE", "year" => 2024, "rating" => 10],
        ];
    }

    /** @test */
    public function filter_with_no_value_returns_without_filtering(): void
    {
        $titleFilter = new TitleFilter('', TitleOperatorsEnum::CONTAINS);
        $filteredMovies = $titleFilter->apply($this->movies);

        $this->assertEquals($this->movies, $filteredMovies);
    }

    /** @test */
    public function filter_contains_bc(): void
    {
        $titleFilter = new TitleFilter('BC', TitleOperatorsEnum::CONTAINS);
        $filteredMovies = $titleFilter->apply($this->movies);

        $this->assertEquals([
            ["title" => "ABC", "year" => 2022, "rating" => 5],
            ["title" => "BCD", "year" => 2023, "rating" => 7]
        ], $filteredMovies);
    }

    /** @test */
    public function filter_starts_with_b(): void
    {
        $titleFilter = new TitleFilter('B', TitleOperatorsEnum::STARTS_WITH);
        $filteredMovies = $titleFilter->apply($this->movies);
        $this->assertEquals([["title" => "BCD", "year" => 2023, "rating" => 7]], $filteredMovies);
    }

    /** @test */
    public function filter_ends_with_e(): void
    {
        $titleFilter = new TitleFilter('E', TitleOperatorsEnum::ENDS_WITH);
        $filteredMovies = $titleFilter->apply($this->movies);
        $this->assertEquals([["title" => "CDE", "year" => 2024, "rating" => 10]], $filteredMovies);
    }
}

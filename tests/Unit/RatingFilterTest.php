<?php

namespace Tests\Unit;

use App\Filters\RatingFilter;
use PHPUnit\Framework\TestCase;
use App\Enums\RatingOperatorsEnum;

class RatingFilterTest extends TestCase
{
    private Array $movies;

    function setUp(): void
    {
        parent::setUp();

        $this->movies = [
            ["title" => "ABC", "year" => 2022, "rating" => 5],
            ["title" => "BCD", "year" => 2023, "rating" => 7],
            ["title" => "CDE", "year" => 2024, "rating" => 10],
        ];
    }
    
    /** @test */
    public function filter_lesser_or_equals_7(): void
    {
        $ratingFilter = new RatingFilter(7, RatingOperatorsEnum::LESSER_OR_EQUALS);
        $filteredMovies = $ratingFilter->apply($this->movies);

        $this->assertEquals([
            ["title" => "ABC", "year" => 2022, "rating" => 5],
            ["title" => "BCD", "year" => 2023, "rating" => 7]
        ], $filteredMovies);
    }
    
    /** @test */
    public function filter_greater_or_equals_7(): void
    {
        $ratingFilter = new RatingFilter(7, RatingOperatorsEnum::GREATER_OR_EQUAL);
        $filteredMovies = $ratingFilter->apply($this->movies);

        $this->assertEquals([
            ["title" => "BCD", "year" => 2023, "rating" => 7],
            ["title" => "CDE", "year" => 2024, "rating" => 10]
        ], $filteredMovies);
    }
}

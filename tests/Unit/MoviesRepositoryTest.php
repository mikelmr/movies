<?php

namespace Tests\Unit;

use App\Enums\RatingOperatorsEnum;
use App\Filters\TitleFilter;
use PHPUnit\Framework\TestCase;
use App\Enums\TitleOperatorsEnum;
use App\Filters\RatingFilter;
use App\Filters\YearFilter;
use App\Repositories\MoviesRepository;

class MoviesRepositoryTest extends TestCase
{
    private Array $movies;
    private MoviesRepository $repository;

    function setUp(): void
    {
        parent::setUp();
        $this->movies = [
            ["title" => "ABC", "year" => 2022, "rating" => 5],
            ["title" => "BCD", "year" => 2023, "rating" => 7],
            ["title" => "CDE", "year" => 2024, "rating" => 10],
        ];
        $this->repository = new MoviesRepository($this->movies);
    }

    /** @test */
    public function return_all_movies_without_filters(): void
    {
        $filteredMovies = $this->repository->filter();

        $this->assertEquals($this->movies, $filteredMovies);
    }

    /** @test */
    public function return_movies_with_title_starting_with_b(): void
    {
        $this->repository->addFilter(new TitleFilter('B', TitleOperatorsEnum::STARTS_WITH));
        $filteredMovies = $this->repository->filter();

        $this->assertEquals([["title" => "BCD", "year" => 2023, "rating" => 7]], $filteredMovies);
    }

    /** @test */
    public function return_movies_from_year_2024(): void
    {
        $this->repository->addFilter(new YearFilter(2024));
        $filteredMovies = $this->repository->filter();

        $this->assertEquals([["title" => "CDE", "year" => 2024, "rating" => 10]], $filteredMovies);
    }

    /** @test */
    public function return_movies_with_rating_greater_or_equals_to_7(): void
    {
        $this->repository->addFilter(new RatingFilter(7, RatingOperatorsEnum::GREATER_OR_EQUAL));
        $filteredMovies = $this->repository->filter();

        $this->assertEquals([
            ["title" => "BCD", "year" => 2023, "rating" => 7],
            ["title" => "CDE", "year" => 2024, "rating" => 10]
        ], $filteredMovies);
    }

    /** @test */
    public function return_movies_with_title_containing_b_and_rating_greater_or_equals_to_7(): void
    {
        $this->repository->addFilter(new TitleFilter('B', TitleOperatorsEnum::CONTAINS));
        $this->repository->addFilter(new RatingFilter(7, RatingOperatorsEnum::GREATER_OR_EQUAL));
        $filteredMovies = $this->repository->filter();

        $this->assertEquals([["title" => "BCD", "year" => 2023, "rating" => 7]], $filteredMovies);
    }

}

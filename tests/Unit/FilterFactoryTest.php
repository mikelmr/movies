<?php

namespace Tests\Unit;

use App\Filters\YearFilter;
use App\Filters\TitleFilter;
use App\Filters\RatingFilter;
use PHPUnit\Framework\TestCase;
use App\Factories\FilterFactory;

class FilterFactoryTest extends TestCase
{
    /** @test */
    public function if_not_filters_returns_empty_array(): void
    {
        $filters = FilterFactory::make([]);

        $this->assertEmpty($filters);
    }

    /** @test */
    public function if_wrong_year_filter_returns_empty_array(): void
    {
        $filters = FilterFactory::make([
            'year' => 'ZZ'
        ]);

        $this->assertEmpty($filters);
    }

    /** @test */
    public function if_year_filter_returns_the_filter(): void
    {
        $filters = FilterFactory::make([
            'year' => 2018
        ]);

        $this->assertCount(1, $filters);
        $this->assertInstanceOf(YearFilter::class, $filters[0]);
    }

    /** @test */
    public function if_year_string_filter_returns_the_filter(): void
    {
        $filters = FilterFactory::make([
            'year' => '2018'
        ]);

        $this->assertCount(1, $filters);
        $this->assertInstanceOf(YearFilter::class, $filters[0]);
    }

    /** @test */
    public function if_just_title_given_returns_empty_array(): void
    {
        $filters = FilterFactory::make([
            'title' => 'A'
        ]);

        $this->assertEmpty($filters);
    }

    /** @test */
    public function if_just_title_filter_given_returns_empty_array(): void
    {
        $filters = FilterFactory::make([
            'ftitle' => 'sw'
        ]);

        $this->assertEmpty($filters);
    }

    /** @test */
    public function if_title_filter_is_not_valid_returns_empty_array(): void
    {
        $filters = FilterFactory::make([
            'ftitle' => 'zz',
            'title' => 'A'
        ]);

        $this->assertEmpty($filters);
    }

    /** @test */
    public function if_title_and_title_filter_returns_the_filter(): void
    {
        $filters = FilterFactory::make([
            'ftitle' => 'sw',
            'title' => 'A'
        ]);
        
        $this->assertCount(1, $filters);
        $this->assertInstanceOf(TitleFilter::class, $filters[0]);
    }

    /** @test */
    public function if_just_rating_given_returns_empty_array(): void
    {
        $filters = FilterFactory::make([
            'rating' => 1
        ]);

        $this->assertEmpty($filters);
    }

    /** @test */
    public function if_just_rating_filter_given_returns_empty_array(): void
    {
        $filters = FilterFactory::make([
            'frating' => 'le'
        ]);

        $this->assertEmpty($filters);
    }

    /** @test */
    public function if_rating_filter_is_not_valid_returns_empty_array(): void
    {
        $filters = FilterFactory::make([
            'frating' => 'zz',
            'rating' => 1
        ]);

        $this->assertEmpty($filters);
    }

    /** @test */
    public function if_rating_value_is_not_valid_returns_empty_array(): void
    {
        $filters = FilterFactory::make([
            'frating' => 'le',
            'rating' => 'zz'
        ]);

        $this->assertEmpty($filters);
    }

    /** @test */
    public function if_rating_and_rating_filter_returns_the_filter(): void
    {
        $filters = FilterFactory::make([
            'frating' => 'le',
            'rating' => 5
        ]);
        
        $this->assertCount(1, $filters);
        $this->assertInstanceOf(RatingFilter::class, $filters[0]);
    }

    /** @test */
    public function if_rating_string_and_rating_filter_returns_the_filter(): void
    {
        $filters = FilterFactory::make([
            'frating' => 'le',
            'rating' => '5'
        ]);
        
        $this->assertCount(1, $filters);
        $this->assertInstanceOf(RatingFilter::class, $filters[0]);
    }
}

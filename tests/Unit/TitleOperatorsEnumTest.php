<?php

namespace Tests\Unit;

use App\Enums\TitleOperatorsEnum;
use PHPUnit\Framework\TestCase;

class TitleOperatorsEnumTest extends TestCase
{
    /** @test */
    public function if_valid_value_returns_ok_with_valid_value(): void
    {
        $fromValue = TitleOperatorsEnum::fromValue(TitleOperatorsEnum::CONTAINS->value);
        $this->assertEquals(TitleOperatorsEnum::CONTAINS, $fromValue);
    }

    /** @test */
    public function if_not_valid_value_returns_null(): void
    {
        $fromValue = TitleOperatorsEnum::fromValue('NOT_VALID');
        $this->assertNull($fromValue);
    }
}

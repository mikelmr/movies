<!doctype html>
<html lang="es">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>{{ config('app.name') }}</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <link rel="stylesheet" href="styles.css">
  </head>
  <body>
    <header>
        <h1 class="text-center">Lista de Películas</h1>
    </header>
    <main>
        <table class="table">
            <thead>
            <tr>
                <th scope="col">Título</th>
                <th scope="col">Año</th>
                <th scope="col">Valoración</th>
                <th scope="col"></th>
            </tr>
            </thead>
            <tbody>
                <tr>
                    <form action="\">
                        <td>
                            <div class="row g-3">
                                <div class="col-auto">
                                    <select name="ftitle" class="form-control">
                                        <option value="c" {{request()->input('ftitle') == 'c' ? 'selected':''}}>Contiene:</option>
                                        <option value="sw" {{request()->input('ftitle') == 'sw' ? 'selected':''}}>Empieza por:</option>
                                        <option value="ew" {{request()->input('ftitle') == 'ew' ? 'selected':''}}>Termina por:</option>
                                    </select>
                                </div>
                                <div class="col">
                                    <input type="text" class="form-control" name="title" placeholder="title filter" aria-describedby="title filter" 
                                        value="{{ request()->input('title') }}">
                                </div>
                            </div>
                        </td>
                        <td>
                            <input type="numeric" class="form-control" name="year" placeholder="year filter" aria-describedby="year filter"
                                value="{{ request()->input('year') }}">
                        </td>
                        <td>
                            <div class="row g-3">
                                <div class="col">
                                    <input type="numeric" class="form-control" name="rating" placeholder="rating filter" aria-describedby="rating filter" 
                                        value="{{ request()->input('rating') }}">
                                </div>
                                <div class="col-auto">
                                    <select name="frating" class="form-control">
                                        <option value="le" {{request()->input('frating') == 'le' ? 'selected':''}}><=</option>
                                        <option value="ge" {{request()->input('frating') == 'ge' ? 'selected':''}}>>=</option>
                                    </select>
                                </div>
                            </div>
                        </td>
                        <td>
                            <button type="submit" class="btn btn-primary">Filtrar</button>
                        </td>
                    </form>
                </tr>
                @foreach ($movies as $movie)
                    <tr>
                        <td>{{$movie['title']}}</td>
                        <td>{{$movie['year']}}</td>
                        <td>{{$movie['rating']}}</td>
                    </tr>
                    @endforeach
            </tbody>
        </table>       
    </main>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
    <script src="main.js"></script>
  </body>
</html>
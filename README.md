# Movies

## Objetivo
El objetivo de esta prueba consiste en crear un catálogo público de películas que se pueda consultar vía HTTP y vía app de consola. Ambos métodos de acceso ofrecerán exactamente la misma funcionalidad.

El modelo de datos de película constará de los siguientes campos:

- Título
- Año de publicación
- Valoración (del 0 al 10)

En este MVP no habrá base de datos, el catálogo de películas estará alojado en memoria. Aún así, dado que la idea es incorporar una base de datos a futuro, la implementación hará uso de "repository pattern".

El catálogo se debe poder filtrar por los siguientes criterios:

- Título: empieza por, contiene, termina en
- Año: coincidencia exacta
- Valoración: mayor igual, menor igual

## Montar la aplicación
Obtener el código y entrar en la carpeta:
```
git clone https://gitlab.com/mikelmr/movies.git
cd movies
```
Generar el fichero .env:
```
php artisan key:generate --ansi
```
Instalar las dependencias de php:
```
composer install
```

## Probar
Para probar yo he utilizado Laravel Sail, pero no es neceario usarla. A continuación se indican los pasos para ejecutarlo tanto con Sail como sin el.

### Sail
Para que funcione es necesario tener instalado docker en la máquina. Para cualquier duda al respecto consultar la documentación sobre [Sail](https://laravel.com/docs/10.x/sail).
#### Test
```
./vendor/bin/sail test
```
#### Ejecutar
```
./vendor/bin/sail up
```
#### Por consola
- Listar todas
```
./vendor/bin/sail artisan movie:list
```
También es posible filtrar. A continuación aparecen ejemplos de los filtros. Aunque aparecen por separado, se pueden combinar.
- Filtrar por año
```
./vendor/bin/sail artisan movie:list --year=2000
```
- Filtrar por título
```
./vendor/bin/sail artisan movie:list --title=a --ftitle=sw
```
- Filtrar por Rating
```
./vendor/bin/sail artisan movie:list --rating=9 --frating=ge
```
### Sin Sail
#### Test
```
./vendor/bin/phpunit
```
#### Ejecutar
```
php artisan serve
```
#### Por consola
- Listar todas
```
php artisan movie:list
```
También es posible filtrar. A continuación aparecen ejemplos de los filtros. Aunque aparecen por separado, se pueden combinar.
- Filtrar por año
```
php artisan movie:list --year=2000
```
- Filtrar por título
```
php artisan movie:list --title=a --ftitle=sw
```
- Filtrar por Rating
```
php artisan movie:list --rating=9 --frating=ge
```
## Creación
En este apartado indica los comandos ejecutados para crear esta aplicación. Estos pasos no hay que volver a lanzarlos, solo se indican con fines académicos.

La creación de esta aplicación se ha realizado ejecutando el comando que recomienda Laravel:
```
composer create-project laravel/laravel movies
```
Laravel 10 por defecto ya viene con Sail, pero hay que indicar qué servicios levantar. Cómo no es necesaria ninguna base de datos ni ningún otro servicio, se ejecuta el comando sin marcar ningún servicio.
```
php artisan sail:install
```